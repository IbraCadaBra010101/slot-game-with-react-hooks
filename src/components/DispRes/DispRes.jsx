import React from 'react';
import './DispRes.css';

const DispRes = (props) => {
    return (
        <div className='disp-res-wrapper'>
            {props.currentScore === null ? null : props.currentScore > 1 && props.counter === 10 ?
                <div className='bet-info'>Ooohh, that's a ${props.currentScore} win!</div> : null}
            {props.currentScore === null ? null : props.currentScore < 1 && props.counter === 10 ?
                <div className='bet-info'>Ooohh, that's a loss!</div> : null}
            <p className='bet-info'>Current credit {props.currentScore} $</p>
            {props.numbersArray.length > 4 ?
                <p className='bet-info'>{10 - props.counter} rounds to play</p> :
                null
            }   <p className='bet-info'>{props.gameInstructions}</p>
        </div>
    );
};

export default DispRes;


