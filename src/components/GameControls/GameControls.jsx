import React from 'react';
import './GameControls.css';

const GameControls = (props) => {

    return (
        <React.Fragment>
            <div className='game-controls'>
            </div>
            <div className='game-controls'>
                <button id='play'
                        onClick={props.randomNumGenerator}
                        disabled={props.lockPlayButton}>{props.textPlayButton}</button>

                <button id='play' onClick={props.onReset}
                        onDoubleClick={props.onReset}
                >Reset
                </button>
                <button id= 'play'>
                    Save my earnings!
                </button>
            </div>
        </React.Fragment>
    );
};

export default GameControls;