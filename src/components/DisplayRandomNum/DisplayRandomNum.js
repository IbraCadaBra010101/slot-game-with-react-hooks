import React from 'react';
import './DisplayRandomNum.css';

const DisplayRandomNum = (props) => {

    return <React.Fragment>
        <div className='result-images'>


            {props.n.length !== 5 ?
                <div>
                    <p className='info-title'> Selected numbers</p>
                    <img className='img-blank' src={props.s[props.traverseIMG.num].img} alt=""/>
                </div> : props.r !== 0 ? props.n.length > 0 && props.n.length !== 5 ?
                    (<div>

                        <p className='info-title'> Selected numbers</p>
                        <img className='img-blank' src={props.s[props.traverseIMG.num].img} alt=""/>
                    </div>) : (<img className='img-blank' src={props.blank[0].img} alt=""/> ?
                        <div>
                            <p className='info-title'>Game generated number</p>
                            <img className='img-blank' src={props.s[props.r - 1].img} alt=""/>
                        </div> : <img className='img-blank' src={props.blank[0].img} alt=""/>) :
                    <div>
                        <p className="info-title"> Select 5 numbers!</p>
                    </div>}


            {/*{props.r !== 0 ? props.n.length > 0 && props.n.length !== 5 ?*/}
            {/*(<div>*/}
            {/*    */}
            {/*    <p className='info-title'> Selected numbers</p>*/}
            {/*    <img className='img-blank' src={props.s[props.traverseIMG.num].img} alt=""/>*/}
            {/*</div>) : (<img className='img-blank' src={props.blank[0].img} alt=""/> ?*/}
            {/*    <div>*/}
            {/*        <p className='info-title'>Game generated number</p>*/}
            {/*        <img className='img-blank' src={props.s[props.r - 1].img} alt=""/>*/}
            {/*    </div> : <img className='img-blank' src={props.blank[0].img} alt=""/>) :*/}
            {/*<div>*/}
            {/*    <p className="info-title"> Select 5 numbers!</p>*/}
            {/*</div>}*/}


        </div>
    </React.Fragment>
};

export default DisplayRandomNum;
