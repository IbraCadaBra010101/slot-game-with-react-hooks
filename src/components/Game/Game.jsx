// hooks and react fragment
import React, {Fragment, useState} from 'react';

//CSS
import './Game.css';

//symbols
import sym1 from '../../Images/sym1.png';
import sym2 from '../../Images/sym2.png';
import sym3 from '../../Images/sym3.png';
import sym4 from '../../Images/sym4.png';
import sym5 from '../../Images/sym5.png';
import sym6 from '../../Images/sym6.png';
import sym7 from '../../Images/sym7.png';
import sym8 from '../../Images/sym8.png';
import sym9 from '../../Images/sym9.png';
import blankImage from '../../Images/blank.png';

// components
import GameControls from '../GameControls/GameControls';
import DisplayRandomNum from '../DisplayRandomNum/DisplayRandomNum';
import DispRes from '../DispRes/DispRes';
import DispSymbol from '../DispSymbol/DispSymbol';
import BetDepositForm from '../BetDepositForm/BetDepositForm';


const Game = () => {
    //props
    const symbolImages = [
        {img: sym1, id: 1,},
        {img: sym2, id: 2,},
        {img: sym3, id: 3,},
        {img: sym4, id: 4,},
        {img: sym5, id: 5,},
        {img: sym6, id: 6,},
        {img: sym7, id: 7,},
        {img: sym8, id: 8,},
        {img: sym9, id: 9,},
    ];

    const blankImages = [{img: blankImage, id: 1,},];

    // hooks, lots of them
    const [numbersArray, setNumArr] = useState([]);
    const [random, setRandom] = useState({randomNumber: 0});
    const [bool, setBool] = useState({active: null});
    const [bet, setBet] = useState({amount: 0});
    const [currentScore, setCurrentScore] = useState({amount: null});
    const [lockButton, setLockButton] = useState({button_active: false,});
    const [traverseIMG, setTravserseIMG] = useState({num: 0});

    // const [fireStore, setFireStore] = useState({winnings: 0});
    const [resetButton, setResetButton] = useState({reset_button_txt: 'Reset', isDisabled: true,});
    const [gameInstructions, setInstructions] = useState({current_txt: 'Begin by picking 5 numbers'});
    const [counter, setCount] = useState(1);
    const [lockPlayButton, setLockPlayButton] = useState({play_button_disabled: true, play_button_txt: 'Play'});
    const [input, setInput] = useState({isDisabled: true});
    const [gameOutcome, setGameOutcome] = useState({win: null,});

    // calculating odds and possible win.
    const divide = numbersArray.length / 0.5;
    const odds = numbersArray.length / 0.1;


    // Limit to three numerals i.e 999 max and only numerals.
    const extract = (str, pattern) =>
        (str.match(pattern) || []).pop() || '';

    const extractNum = (str, length) =>
        extract(str, "^(0|[1-9][0-9]*)$").substring(0, length);


    const randomNumGenerator = () => {
        setLockPlayButton({
            play_button_disabled: false,
            play_button_txt: 'Wait for it ...',
        });

        setCount(counter + 1);

        setTimeout(() => {
            if (numbersArray.includes(random.randomNumber)) {
                setCurrentScore({amount: Number(bet.amount) * 2})
            }
            if (!numbersArray.includes(random.randomNumber)) {
                setCurrentScore({amount: 0})
            }

            setRandom(() => {
                const r = Math.floor(Math.random() * 9 + 1);
                return {randomNumber: r}
            });

            const confirmSelection = numbersArray.map(el => {
                return el + ' ';
            });
            setInstructions({current_txt: `You selected the following: ${confirmSelection}`});
            if (counter > 0) {
                setInput({isDisabled: true})
            }
            if (counter === 9) {
                setLockPlayButton({play_button_disabled: true, play_button_txt: 'Reset game to play'});
            } else {
                setLockPlayButton({
                    play_button_disabled: false,
                    play_button_txt: 'Play'
                });
            }
        }, 1000);
    };

    const onSelectSymbol = (i) => {
        setTravserseIMG({num: i - 1});
        setBool({active: i,});
        if (numbersArray.length === 0) {
            setInstructions({current_txt: 'Remember, you can play 9 rounds no matter the score'});
        }
        if (numbersArray.length === 1) {
            setInstructions({
                current_txt: 'However, you dont want to be greedy,' +
                    'withdraw earnings before it  be to late'
            });
        }
        if (numbersArray.length === 4) {
            setInstructions({
                current_txt: 'Enter a sum and play to see your selected numbers'
            });
        }
        if (numbersArray.length === 4) {
            setLockButton({
                button_active: true,
            });
            setLockPlayButton({play_button_disabled: false, play_button_txt: 'Play'});
            setInput({isDisabled: false,})
        }
        return numbersArray.length < 5 && !numbersArray.includes(i)
            ? setNumArr([...numbersArray, i]) : null;
    };
    const onReset = () => {
        // resetting all hooks to origin
        setNumArr([]);
        setRandom({randomNumber: 0});
        setBool({active: null,});
        setBet({amount: 0});
        setCurrentScore({amount: null});
        setLockButton({button_active: false,});
        setTravserseIMG({num: null,});
        setResetButton({reset_button_txt: 'Reset', isDisabled: true});
        setInstructions({current_txt: 'Begin by picking 5 numbers'});
        setCount(1);
        setLockPlayButton({play_button_disabled: true, play_button_txt: 'Play'});
        setInput({isDisabled: true});
        setGameOutcome({win: null,})
    };

    return <Fragment>

        <DisplayRandomNum
            r={random.randomNumber}
            s={symbolImages}
            n={numbersArray}
            blank={blankImages}
            traverseIMG={traverseIMG}
            score={currentScore}
        />

        <DispRes
            numbersArray={numbersArray}
            bet={bet.amount}
            divide={divide}
            odds={odds}
            currentScore={currentScore.amount}
            gameInstructions={gameInstructions.current_txt}
            gameOutcome={gameOutcome}
            counter={counter}
        />

        <div className='img-wrapper'>
            {
                symbolImages.map((o, index) => {
                    return <DispSymbol key={index}
                                       index={index + 1} id={o.id} img={o.img}
                                       onSelectSymbol={onSelectSymbol}
                                       isActive={bool.active === index + 1}
                                       isLocked={lockButton.button_active}
                                       numbersArray={numbersArray}
                    />
                })
            }
        </div>
        <BetDepositForm
            setBet={setBet}
            bet={bet}
            extractNum={extractNum}
            input={input}
            counter={counter}

        />

        <GameControls randomNumGenerator={randomNumGenerator}
                      n={numbersArray}
                      lockPlayButton={lockPlayButton.play_button_disabled}
                      textPlayButton={lockPlayButton.play_button_txt}
                      resetButton={resetButton}
                      r={random.randomNumber}
                      onReset={onReset}
        />

    </Fragment>
};

export default Game;














