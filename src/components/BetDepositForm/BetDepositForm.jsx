import React from 'react';
import './BetDepositForm.css'

const BetDepositForm = (props) => {

    return (
        <form action=""
              className='formHandler'
              onSubmit={props.betHandler}>

            <input type="text" onChange={(e) => {
                props.setBet({amount: props.extractNum(e.target.value, 3)})
            }}
                   value={props.bet.amount}
                   disabled={props.input.isDisabled}/>
        </form>
    );
};

export default BetDepositForm;