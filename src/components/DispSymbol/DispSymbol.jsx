import React from 'react';
import './DispSymbols.css';

const DispSymbol = (props) => {
    const selectIMG = () => {
        props.onSelectSymbol(props.index, 250);
    };
    return (
        <div>
            {
                props.numbersArray.length !== 5 ? <div>
                    <img disabled={props.isLocked}
                         className={props.isActive ? 'selected' : 'unselected'}
                         src={props.img}
                         onClick={selectIMG}
                         alt='missing'/>
                </div> : null
            }
            {
                props.numbersArray.length > 4 ? <div>
                    <img disabled={props.isLocked}
                         className='unselected'
                         src={props.img}
                         onClick={selectIMG}
                         alt='missing'/>
                </div> : null
            }
        </div>

    );
};

export default DispSymbol;